package com.majoriebash.bugsndrugs.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.db.entities.ReportedBugs
import com.majoriebash.bugsndrugs.data.db.entities.ReportedDrugs
import java.text.DateFormat

class DrugsAdapter(patients:ArrayList<ReportedDrugs>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<ReportedDrugs>? = patients
    private var mRecyclerViewItems1: ArrayList<ReportedDrugs>? = patients
    private var bugsFiltered: MutableList<ReportedDrugs> = ArrayList()

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var age: TextView = view.findViewById(R.id.age)
        var gender: TextView = view.findViewById(R.id.gender)
        var reference: TextView = view.findViewById(R.id.reference)
        var date: TextView = view.findViewById(R.id.registrationDate)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bugs, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder
            val item: ReportedDrugs = mRecyclerViewItems!![position]
            val created =
                DateFormat.getDateInstance(DateFormat.MEDIUM)
                    .format(item.dateCreated!!)
            menuItemHolder.age.text = "${item.age} Years with ${item.diagnosis} Diagnosis"
            menuItemHolder.gender.text = "${item.gender}"
            menuItemHolder.reference.text = "ID: ${item.uniqueId}"
            menuItemHolder.date.text = "Registered on $created"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                bugsFiltered = if (charString.isNullOrEmpty()) {
                    val filteredList: ArrayList<ReportedDrugs> = ArrayList()
                    for (row in mRecyclerViewItems1!!) {
                        filteredList.add(row)
                    }
                    filteredList
                } else {
                    val filteredList: ArrayList<ReportedDrugs> = ArrayList()
                    for (row in mRecyclerViewItems1!!) {

                        if (row.hospitalNumber!!.toLowerCase()
                                .contains(charString.toLowerCase()) || row.age.toString().contains(charSequence) ||
                                row.diagnosis!!.toLowerCase().toString().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = bugsFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                mRecyclerViewItems!!.clear()
                mRecyclerViewItems!!.addAll(filterResults.values as ArrayList<ReportedDrugs>)
                notifyDataSetChanged()
            }
        }
    }
}