package com.majoriebash.bugsndrugs.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.majoriebash.bugsndrugs.R

class AddDrugsAdapter(patients:ArrayList<String>?, routes:ArrayList<String>?,doses:ArrayList<String>?,
                      frequency:ArrayList<String>?,durations:ArrayList<String>?):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<String>? = patients
    private var mRecyclerViewItems1: ArrayList<String>? = routes
    private var mRecyclerViewItems2: ArrayList<String>? = doses
    private var mRecyclerViewItems3: ArrayList<String>? = frequency
    private var mRecyclerViewItems4: ArrayList<String>? = durations

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.drug)
        var route:TextView = view.findViewById(R.id.route)
        var dose:TextView = view.findViewById(R.id.dose)
        var freq:TextView = view.findViewById(R.id.frequency)
        var dura:TextView = view.findViewById(R.id.duration)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.drugitem, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: String = mRecyclerViewItems!![position]
            val item1: String = mRecyclerViewItems1!![position]
            val item2: String = mRecyclerViewItems2!![position]
            val item3: String = mRecyclerViewItems3!![position]
            val item4: String = mRecyclerViewItems4!![position]

            menuItemHolder.name.text = item
            menuItemHolder.route.text = item1
            menuItemHolder.dose.text = item2
            menuItemHolder.freq.text = item3
            menuItemHolder.dura.text = item4
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}