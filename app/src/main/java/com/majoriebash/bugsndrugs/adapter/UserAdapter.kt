package com.majoriebash.bugsndrugs.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.util.Users
import com.majoriebash.bugsndrugs.util.activated
import com.majoriebash.bugsndrugs.util.user_name

class UserAdapter(patients:ArrayList<Auth>?,context:Context): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<Auth>? = patients
    var accounts:ArrayList<String> = ArrayList()
    var mContext = context

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.name)
        var email: TextView = view.findViewById(R.id.email)
        var organization: TextView = view.findViewById(R.id.orgname)
        var switch:Switch = view.findViewById(R.id.pinSwitch)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: Auth = mRecyclerViewItems!![position]
            menuItemHolder.name.text = "${item.name}"
            menuItemHolder.email.text = "${item.email}"
            menuItemHolder.organization.text = "${item.insitution}"
            menuItemHolder.switch.isChecked = item.activated!!

            menuItemHolder.switch.setOnClickListener{
                if(!accounts.contains(item.name) && item.activated==true) {
                    val dialog = AlertDialog.Builder(mContext)
                    dialog.setTitle("Deactivate Account")
                    dialog.setMessage("Your about to change this account status")
                    dialog.setCancelable(false)
                    dialog.setPositiveButton("Continue") { dialogBox, _ ->
                        accounts.add(item.name!!)
                        Firebase().deactivateAccount(
                            item.email!!,
                            Users,
                            activated,
                            user_name,
                            false
                        )
                        Toast.makeText(
                            mContext,
                            "Account Deactivated Successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                        dialogBox.dismiss()

                    }
                    dialog.setNegativeButton("Cancel") { dialogBox, _ ->
                        menuItemHolder.switch.isChecked = item.activated!!
                        dialogBox.dismiss()
                    }.show()
                }else{
                    Toast.makeText(
                        mContext,
                        "This account has been deactivated, contact system administrator",
                        Toast.LENGTH_LONG
                    ).show()
                    menuItemHolder.switch.isChecked = false
                }
            }

        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}