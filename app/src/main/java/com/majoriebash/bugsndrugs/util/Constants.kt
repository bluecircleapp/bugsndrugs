package com.majoriebash.bugsndrugs.util

const val nanoseconds = "nanoseconds"
var myFormat = "dd/MM/yy"
var myFormat1 = "E dd-MMM-yyyy"
const val seconds = "seconds"
const val Users = "User"
const val Bugs = "Bugs"
const val Drugs = "Drugs"
const val reportedBugs = "ReportedBugs"
const val reportedDrugs = "ReportedDrugs"
const val BugsAntibiotics = "BugsAntibiotics"
const val Initialized = "Initialized"
const val Update = "Update"
const val Version = "Version"
const val Url = "Url"
const val Versioning = "1.02"
const val Reauth = "reauth"
const val valid ="valid"
const val Pin = "Pin"
const val SetPin = "Pin_Active"
const val active = "Active"
const val Switch = "switch"

const val user_name = "Email"
const val pass_word = "Password"
const val activated = "Activated"
const val roleId = "Role"
const val organization = "Institution"
const val address_org = "Address of Institution"
const val state_org = "State"
const val profession_org = "Profession"
const val name = "FullName"
const val privilege = "Access"
const val phone = "Phone Number"
const val lgas = "Local Government Area"
const val date_created = "Date Created"
const val Type = "Type"
const val Name = "Name"
const val Desc = "Desc"
const val Sync= "Sync"

const val Age = "Age"
const val UniqueId = "Unique_Id"
const val Diagnosis = "Working Diagnosis"
const val Gender = "Gender"
const val Hospital = "Hospital Number"
const val PatientSetting = "Patient Setting"
const val SpecimenType = "Specimen Type"
const val AntibioticsId = "Antibiotics_Id"
const val created_by = "Created By"
const val Antibiotics = "Antibiotics"
const val Sensitivity = "Sensitivity"
const val Dose = "Dose"
const val Duration = "Duration"
const val Frequency = "Frequency"
const val Route="Route"
const val Amount="Amount"



