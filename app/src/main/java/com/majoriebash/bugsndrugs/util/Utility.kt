package com.majoriebash.bugsndrugs.util

class Utility {

    fun getUniqueID():String?{
        return "${randomString(5)}${randomInt(5)}"
    }

    fun randomString(size: Int): String {
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return (source).map { it }.shuffled().subList(0, size).joinToString("")
    }
    fun randomInt(size: Int): String {
        val source = "1234567890"
        return (source).map { it }.shuffled().subList(0, size).joinToString("")
    }
}