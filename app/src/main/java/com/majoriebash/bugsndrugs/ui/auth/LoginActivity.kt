package com.majoriebash.bugsndrugs.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.databinding.ActivityLoginBinding
import com.majoriebash.bugsndrugs.ui.home.HomeActivity
import com.majoriebash.bugsndrugs.util.Reauth
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import com.majoriebash.bugsndrugs.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class LoginActivity : AppCompatActivity(),KodeinAware,AuthListener {
    override val kodein by kodein()

    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: AuthViewModel
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProvider(this,factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.authListener = this

        viewmodel.auth.observe(this, Observer {
            if(it==null) {
                viewmodel.checkAuthentication()
            }
        })
    }

    override fun onStarted(id:Int) {
        if(id==1) {
            binding.login.hide()
            binding.progressBar.show()
        }else if(id == 3){
            onForgetDisplay()
        }
        else{
            Intent(this, RegisterActivity::class.java).also {
                startActivity(it)
            }
        }
    }

    override fun onSuccess(message: String?) {
        toast(message!!)
        Intent(this,HomeActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }

    override fun onError(message: String?) {
        toast(message!!)
    }

    override fun onStopped() {
        binding.login.show()
        binding.progressBar.hide()
    }

    override fun onFirebaseFinish(response: LiveData<Any>, i: Int) {
        if(i==1) {
            response.observe(this, Observer {
                viewmodel.loginUser(it as String)
            })
        }else if(i==2){
            response.observe(this, Observer {
                if(it as Boolean){
                    if(it){
                        prefs.saveBooleanPreference(Reauth,true)
                        prefs.deleteAll()
                        onError("Email Sent")
                    }else{
                        onError("The email does not exist on record.")
                    }
                }else{
                    onError("Email was not sent. Check network connection")
                }
            })
        }
    }
    private fun onForgetDisplay() {
        val view = LayoutInflater.from(this).inflate(R.layout.reset_password, null) as View
        val editText: EditText = view.findViewById(R.id.resetPassword)
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Forgot Password")
        dialog.setView(view)
        dialog.setCancelable(false)
        dialog.setPositiveButton("Reset") { dialogBox, _ ->
            viewmodel.resetPassword(editText.text.toString())
            dialogBox.dismiss()
        }
        dialog.setNegativeButton("Cancel") { dialogBox, _ -> dialogBox.dismiss()
        }.show()
    }
}
