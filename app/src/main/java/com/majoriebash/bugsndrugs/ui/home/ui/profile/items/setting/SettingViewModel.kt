package com.majoriebash.bugsndrugs.ui.home.ui.profile.items.setting

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.util.*
import org.json.JSONObject

class SettingViewModel(
    private val prefs:PreferenceProvider
): ViewModel() {
    var pinChecked:Boolean? = null
    var listener:SettingListener? = null

    var auths:MutableLiveData<List<Auth>> = MutableLiveData<List<Auth>>()

    private val _role = MutableLiveData<String?>().apply {
        value = prefs.getLastSavedAt(roleId)
    }
    private val _pin = MutableLiveData<Boolean?>().apply {
        value = prefs.getLastBoolean(SetPin)
    }

    val knowRole: LiveData<String?> = _role
    val knowPin: LiveData<Boolean?> = _pin

    fun onChangePassword(view: View){
        listener?.onStarted(1)
    }
    fun onChangeNumber(view: View){
        listener?.onStarted(2)
    }
    fun onChangePin(view: View){
        listener?.onStarted(3)
    }
    fun onDeactivate(view:View){
        listener?.onStarted(4)
    }
    fun onPinSwitch(view: View){
        if(pinChecked!!){
            listener?.onStarted(3)
        }else{
            listener?.onError("Pin login disabled")
            prefs.saveBooleanPreference(SetPin,pinChecked)
        }
    }
    fun changePassword(oldPass:String?, newPass:String?){
        val response = Firebase().changeUserPassword(oldPass!!,newPass!!)
        listener?.onFirebaseFinish(response, 1)
    }
    fun changePhone(number:String?){
        val response = Firebase().updateDocument(prefs.getLastSavedAt(user_name)!!, Users, phone,user_name,number!!)
        listener?.onFirebaseFinish(response, 2)
    }
    fun pinSet(pin:String?,pinState:Boolean){
        if(pin!!.length==6) {
            listener?.onSuccess("Pin set successfully")
            prefs.saveBooleanPreference(SetPin, pinState)
            prefs.savePreference(Pin, pin)
        }else{
            listener?.onSuccess("Pin is less than 6 digits")
        }
    }
    fun passwordChanged(data:Boolean){
        if(data){
            listener?.onSuccess("User Password Changed")
            prefs.saveBooleanPreference(Reauth,true)
        }else{
            listener?.onError("User password cannot be changed at this moment. Try again")
        }
    }
    fun phoneChanged(data:Boolean){
        if(data){
            listener?.onSuccess("Phone Number Changed")
        }else{
            listener?.onError("Phone number cannot be changed at this moment. Try again")
        }
    }

    fun getUsers(){
        val response = Firebase().getDocument(Users)
        listener?.onFirebaseFinish(response, 1)
    }

    fun processUsers(data: QuerySnapshot){
        if(!data.isEmpty) {
            val authList:ArrayList<Auth> = ArrayList<Auth>()
            data.forEach {
                val document = Gson().toJson(it.data)
                val json = JSONObject(document)
                val auth =
                    Auth(
                        1,
                        json.getString(name),
                        json.getString(organization),
                        json.getString(address_org),
                        json.getString(profession_org),
                        json.getString(state_org),
                        json.getString(user_name).trim(),
                        json.getString(lgas),
                        json.getString(roleId),
                        json.getString(privilege),
                        json.getString(phone),
                        Converter().timeStamptoDate(json.getString(date_created)),
                        json.getBoolean(activated)
                    )
                authList.add(auth)
            }
            auths.apply {
                value = authList
            }
        }else{
            listener?.onError("An error occurred, Please check internet!!")
        }
    }

}