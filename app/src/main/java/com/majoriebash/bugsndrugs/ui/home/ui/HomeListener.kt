package com.majoriebash.bugsndrugs.ui.home.ui

import androidx.lifecycle.LiveData

interface HomeListener {
    fun onStarted()
    fun onClickEvent(id:Int)
    fun onSuccess(name:String?)
    fun onError(message:String?)
    fun onFirebaseFinish(response: LiveData<Any>,id:Int)
}