package com.majoriebash.bugsndrugs.ui.pages

import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.adapter.DrugsAdapter
import com.majoriebash.bugsndrugs.data.db.entities.ReportedDrugs
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.databinding.ActivityBugsBinding
import com.majoriebash.bugsndrugs.databinding.InitializeLayoutBinding
import com.majoriebash.bugsndrugs.util.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File

class DrugsActivity : AppCompatActivity(),KodeinAware,PageListener {

    override val kodein by kodein()

    private val factory: PagesViewModelFactory by instance<PagesViewModelFactory>()
    private val prefs:PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: PagesViewModel
    private var listPatient:ArrayList<ReportedDrugs>? = ArrayList()
    private var patientAdapter: DrugsAdapter? = DrugsAdapter(listPatient)
    private var alertDialog: AlertDialog.Builder? = null
    private var alert: AlertDialog? = null
    private lateinit var binding: ActivityBugsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Drugs Records"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_bugs)
        viewmodel = ViewModelProvider(this,factory).get(PagesViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.pageListener = this

        viewmodel.drugs.observe(this, Observer {int->
            if(int.isNotEmpty()){
                listPatient?.clear()
                for(e in int.indices){
                    listPatient?.add(int[e])
                }
                patientAdapter?.notifyDataSetChanged()
            }
        })

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = patientAdapter
            addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    binding.recyclerView,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            if(listPatient!!.isNotEmpty()) {
                                val patient = listPatient!![position]
                                prefs.savePreference(UniqueId, patient.uniqueId)
                                prefs.savePreference(Switch, "2")
                                Intent(context, EditRecordActivity::class.java).also {
                                    startActivity(it)
                                }
                            }
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.search_menu, menu)

        // Associate searchable configuration with the SearchView

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.maxWidth = Int.MAX_VALUE

        // listening to search query text change

        // listening to search query text change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                // filter recycler view when query submitted
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                // filter recycler view when text is changed
                viewmodel.drugs.observe(this@DrugsActivity, Observer {int->
                    if(int.isNotEmpty()){
                        listPatient?.clear()
                        for(e in int.indices){
                            listPatient?.add(int[e])
                        }
                        patientAdapter?.notifyDataSetChanged()
                    }
                })
                patientAdapter!!.getFilter()!!.filter(query)
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true
        }
        if(id==R.id.export){
            val dialog = AlertDialog.Builder(this)
            dialog.setTitle("Export to CSV")
            dialog.setMessage("To export accurate data, update records before continuing \n\nGo to Profile -> Synchronize Data ")
            dialog.setCancelable(false)
            dialog.setPositiveButton("Cancel Export", DialogInterface.OnClickListener(){
                    dialogBox, _ ->
                finish()
                dialogBox.dismiss()
            })
            dialog.setNegativeButton("Continue Export", DialogInterface.OnClickListener(){
                    dialogBox, _ ->
                val view = InitializeLayoutBinding.inflate(layoutInflater)
                view.name.hide()
                view.synctext.text = "Please Wait while we export to excel"
                alertDialog = AlertDialog.Builder(this)
                alertDialog?.setView(view.root)
                alertDialog?.setCancelable(false)
                alert = alertDialog?.create()
                alert?.show()
                viewmodel.drugsCSV.observe(this, Observer {
                    viewmodel.exportDrugsToCsv(it, this)
                })
                dialogBox.dismiss()
            }).show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    override fun openFile(file: File) {
        Handler().postDelayed({
            alert?.dismiss()
            val packageManager: PackageManager = this.packageManager
            val testIntent = Intent(Intent.ACTION_VIEW)
            testIntent.type = "application/vnd.ms-excel"
            val list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY)
            if (list.size > 0 && file.isFile) {
                val install = Intent(Intent.ACTION_VIEW)
                install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                val apkURI: Uri = FileProvider.getUriForFile(
                    this, this
                        .packageName.toString() + ".provider", file
                )
                install.setDataAndType(apkURI, "application/vnd.ms-excel")
                install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                startActivity(install)
            }else{
                toast("Please check if you can view excel files")
            }
        },3000L)

    }

    override fun onError() {
        Handler().postDelayed({
            alert?.dismiss()
            toast("An error occurred exporting data to excel")
        },3000L)
    }
}
