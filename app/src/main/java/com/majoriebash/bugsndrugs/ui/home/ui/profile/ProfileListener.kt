package com.majoriebash.bugsndrugs.ui.home.ui.profile

import androidx.lifecycle.LiveData

interface ProfileListener {
    fun onStarted(id:Int?)
    fun onSuccess(message:String?)
    fun onError(message:String?)
    fun onFirebaseFinish(response: LiveData<Any>,id:Int)
}