package com.majoriebash.bugsndrugs.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.majoriebash.bugsndrugs.R
import com.majoriebash.bugsndrugs.databinding.ActivityRegisterBinding
import com.majoriebash.bugsndrugs.ui.home.HomeActivity
import com.majoriebash.bugsndrugs.util.hide
import com.majoriebash.bugsndrugs.util.show
import com.majoriebash.bugsndrugs.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class RegisterActivity : AppCompatActivity(),KodeinAware, AuthListener, AdapterView.OnItemSelectedListener {

    override val kodein by kodein()

    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()
    private lateinit var viewmodel: AuthViewModel
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        viewmodel = ViewModelProvider(this,factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        binding.lga.onItemSelectedListener = this
        binding.profession.onItemSelectedListener = this
        binding.state.onItemSelectedListener = this
        viewmodel.authListener = this
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onStarted(id: Int) {
        if(id==1){
            Intent(this,LoginActivity::class.java).also {
                startActivity(it)
                finish()
            }
        }
        if(id==2){
            binding.register.hide()
            binding.progressBar.show()
        }
    }

    override fun onSuccess(message: String?) {
        toast(message!!)
        Intent(this, HomeActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }

    override fun onError(message: String?) {
        toast(message!!)
    }

    override fun onStopped() {
        binding.register.show()
        binding.progressBar.hide()
    }

    override fun onFirebaseFinish(response: LiveData<Any>, i: Int) {
        if(i==1) {
            response.observe(this, Observer {
                if (it as Boolean) {
                    onError("User Account Created Successfully")
                    viewmodel.loginWithDetails()
                } else {
                    onError("Cannot create users at this moment")
                }
            })
        }
        if(i==2){
            response.observe(this, Observer {
                viewmodel.loginUser(it as String)
            })
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent!!.id){
            R.id.lga->if(!parent.selectedItem.toString().equals("select lga",ignoreCase = true))
            {viewmodel.lga = parent.selectedItem.toString()}
            R.id.profession->if(parent.selectedItem.toString().equals("other",ignoreCase = true))
            {binding.professionText.show() }else{
                binding.professionText.hide()
                if(!parent.selectedItem.toString().equals("select profession",ignoreCase = true)){
                    viewmodel.profession = parent.selectedItem.toString()
                }
            }
            R.id.state->if(parent.selectedItem.toString().equals("abia",ignoreCase = true))
            {binding.stateText.hide()
                viewmodel.state = parent.selectedItem.toString()
                binding.lga.show()}else{
                if(!parent.selectedItem.toString().equals("select state",ignoreCase = true)){
                    binding.stateText.show()
                    binding.lga.hide()
                    viewmodel.state = parent.selectedItem.toString()
                }
            }
        }
    }
}
