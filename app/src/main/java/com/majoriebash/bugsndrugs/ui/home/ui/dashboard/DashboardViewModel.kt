package com.majoriebash.bugsndrugs.ui.home.ui.dashboard

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.majoriebash.bugsndrugs.data.db.entities.Bugs
import com.majoriebash.bugsndrugs.data.db.entities.Drugs
import com.majoriebash.bugsndrugs.data.db.entities.ReportedBugs
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.ui.home.ui.HomeListener
import com.majoriebash.bugsndrugs.util.*
import org.json.JSONObject
import java.sql.Date
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DashboardViewModel(
    private val prefs:PreferenceProvider,
    private val repository: DataRepository
) : ViewModel() {
    val bugs = repository.getBugs()
    val drugs = repository.getDrugs("1")

    var age:String? = null
    var gender:String? = null
    var patientSetting:String? = null
    var bugsSelect:String? = null
    var diagnosis:String? = null
    var specimen:String? = null
    var hospital:String? = null

    var homeListener:HomeListener? = null

    fun onRefreshClick(view: View){
        homeListener?.onStarted()
        val response = Firebase().getDocument(Bugs)
        homeListener?.onFirebaseFinish(response,1)
    }

    fun getBugs(bugsList:List<Bugs>): ArrayList<String> {
        val bugsHash:HashMap<Int,String?> = HashMap()
        val programs = ArrayList<String>()
        programs.add("Select Bugs")
        if(bugsList.isNotEmpty()) {
            for (i in bugsList.indices) {
                bugsHash[i+1] = bugsList[i].id
                programs.add(bugsList[i].name!!)
            }
        }
        programs.add("Other")
        return programs
    }
    fun getDrugs(bugsList:List<Drugs>): ArrayList<String> {
        val bugsHash:HashMap<Int,String?> = HashMap()
        val programs = ArrayList<String>()
        programs.add("Select Antibiotics")
        if(bugsList.isNotEmpty()) {
            for (i in bugsList.indices) {
                bugsHash[i+1] = bugsList[i].id
                programs.add(bugsList[i].name!!)
            }
        }
        return programs
    }
    fun processBugs(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val bugs =
                    com.majoriebash.bugsndrugs.data.db.entities.Bugs(it.id,
                        obj.getString(Name),
                        obj.optString(Desc))
                Coroutines.main{
                    repository.saveBugs(bugs)
                }
            }
            homeListener?.onError("List refreshed successfully")
        }else{
            homeListener?.onError("Please check internet connection!!")
        }
    }
    fun processData(antibiotics:ArrayList<String>?, sensitivity:ArrayList<String>?):Boolean{
        if(age.isNullOrEmpty() || gender.isNullOrEmpty() || patientSetting.isNullOrEmpty() || diagnosis.isNullOrEmpty() || specimen.isNullOrEmpty()
            || bugsSelect.isNullOrEmpty() || hospital.isNullOrEmpty()){
            homeListener?.onError("Some fields are empty and cannot be submitted")
            return false
        }else if(antibiotics?.size!=sensitivity?.size){
            homeListener?.onError("Some fields are empty and cannot be submitted")
            return false
        }
        else {
            val todayDOB = Date(Calendar.getInstance().timeInMillis)

            val reportedBugs = ReportedBugs(
                null, Utility().getUniqueID(), age?.toInt(), gender, hospital,patientSetting, diagnosis, specimen,
                bugsSelect,antibiotics.toString(), sensitivity.toString(),todayDOB, prefs.getLastSavedAt(user_name), true
            )
            Coroutines.main {
                repository.saveReportedBugs(reportedBugs)
            }
            homeListener?.onSuccess("Entry has been saved")
            firebaseJob(reportedBugs)
            return true
        }
    }

    private fun firebaseJob(documentBugs:ReportedBugs){

        val reported = HashMap<String, Any?>()
        reported[UniqueId] = documentBugs.uniqueId
        reported[Age] = documentBugs.age
        reported[Gender] = documentBugs.gender
        reported[Hospital] = documentBugs.hospitalNumber
        reported[PatientSetting] = documentBugs.patientSetting
        reported[Diagnosis] = documentBugs.diagnosis
        reported[SpecimenType] = documentBugs.specimentType
        reported[Bugs] = documentBugs.bugs
        reported[Antibiotics] = documentBugs.antibiotic
        reported[Sensitivity] = documentBugs.sensitivity
        reported[date_created] = documentBugs.dateCreated
        reported[created_by] = documentBugs.createdBy
        reported[organization] = prefs.getLastSavedAt(organization)
        reported[active] = true

        Firebase().createDocument(reportedBugs,reported)
    }
}