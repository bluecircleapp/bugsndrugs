package com.majoriebash.bugsndrugs.ui.home.ui.profile

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.majoriebash.bugsndrugs.data.db.entities.ReportedBugs
import com.majoriebash.bugsndrugs.data.db.entities.ReportedDrugs
import com.majoriebash.bugsndrugs.data.firebase.Firebase
import com.majoriebash.bugsndrugs.data.preferences.PreferenceProvider
import com.majoriebash.bugsndrugs.data.repositories.DataRepository
import com.majoriebash.bugsndrugs.util.*
import org.json.JSONObject
import java.sql.Date
import java.util.*
import kotlin.collections.HashMap

class ProfileViewModel(
    private val prefs:PreferenceProvider,
    private val repository: DataRepository
) : ViewModel() {
    var auth = repository.getUser(prefs.getLastSavedAt(user_name)!!)

    var fullname:String? =null
    var email:String? = null
    var orgName:String? = null
    var address:String? = null
    var profession:String? = null
    var state:String? = null
    var role:String? = null
    var number:String? = null
    var lga:String? = null
    var access:String? = null
    var password:String? = null
    var cpassword:String? = null
    var profileListener:ProfileListener? =null

    private val _role = MutableLiveData<String?>().apply {
        value = prefs.getLastSavedAt(roleId)
    }

    val knowRole: LiveData<String?> = _role

    private val _sync = MutableLiveData<String?>().apply {
        value = prefs.getLastSavedAt(Sync)
    }

    val knowSync: LiveData<String?> = _sync

    fun onCreateUser(view: View){
        profileListener?.onStarted(1)
    }
    fun onSettingClick(view: View){
        profileListener?.onStarted(2)
    }
    fun onUpdateClick(view:View){
        profileListener?.onStarted(3)
    }
    fun onDownloadClick(view:View){
        profileListener?.onStarted(2)
    }
    fun onSyncClick(view:View){
        profileListener?.onStarted(4)
        if(!prefs.getLastSavedAt(roleId).equals("owner",true)) {
            val response = Firebase().getDocumentIdentifier(reportedBugs, prefs.getLastSavedAt(organization)!!, organization)
            profileListener?.onFirebaseFinish(response, 4)
        }else{
            val response = Firebase().getDocument(Users)
            profileListener?.onFirebaseFinish(response, 6)
            //val response = Firebase().getDocument(reportedBugs)
            //profileListener?.onFirebaseFinish(response, 4)
        }
    }
    fun onCreateClicked(view:View){
        val todayDOB = Date(Calendar.getInstance().timeInMillis)
        profileListener?.onStarted(0)
        val user = HashMap<String, Any?>()
        user[name] = fullname
        user[organization] = orgName?.toLowerCase()
        user[user_name] = email
        user[phone] = number
        user[address_org] = address
        user[profession_org] = profession
        user[state_org] = state
        user[privilege] = access
        user[roleId] = role
        user[lgas] = lga
        user[date_created] = todayDOB
        user[activated] = true
        if(fullname.isNullOrEmpty() || orgName.isNullOrEmpty() || email.isNullOrEmpty() || role.isNullOrEmpty() ||
            password.isNullOrEmpty() || cpassword.isNullOrEmpty() || access.isNullOrEmpty() || number.isNullOrEmpty()
            || lga.isNullOrEmpty() || state.isNullOrEmpty() || address.isNullOrEmpty() || profession.isNullOrEmpty()){
            profileListener?.onError("Cannot submit with empty fields")
            return
        }
        if(!password.equals(cpassword,true)){
            profileListener?.onError("Password Doesn't Match")
            return
        }
        val response = Firebase().createUser(email!!,password!!,user)
        profileListener?.onFirebaseFinish(response,1)
    }
    fun signoutUser(view: View){
        val response = Firebase().signOut()
        profileListener?.onFirebaseFinish(response,2)
    }
    fun getUpdate(){
        val response = Firebase().getDocument(Update)
        profileListener?.onFirebaseFinish(response,3)
    }
    fun processUpdate(data: QuerySnapshot){
        if(!data.isEmpty) {
            var auth:String = ""
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                auth = obj.getString(Version)
                prefs.savePreference(Url,obj.getString(Url))
            }
            if(Versioning.equals(auth,true)) {
                profileListener?.onStarted(1)
            }else{
                profileListener?.onSuccess(auth)
            }
        }else{
            profileListener?.onError("")
        }
    }

    fun processReportedBugs(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                if(obj.getBoolean(active)) {
                    val reportedBugs = ReportedBugs(null, obj.optString(UniqueId), obj.getInt(Age), obj.optString(Gender),
                        obj.optString(Hospital), obj.optString(PatientSetting), obj.optString(Diagnosis), obj.optString(SpecimenType),
                        obj.optString(Bugs), obj.optString(Antibiotics), obj.optString(Sensitivity), Converter().timeStamptoDate(obj.optString(date_created)),
                        obj.optString(created_by), obj.getBoolean(active)
                    )
                    Coroutines.main {
                        repository.checkSaveBugs(reportedBugs)
                    }
                }
            }
            if(!prefs.getLastSavedAt(roleId).equals("owner",true)) {
                val response = Firebase().getDocumentIdentifier(
                    reportedDrugs,
                    prefs.getLastSavedAt(organization)!!,
                    organization
                )
                profileListener?.onFirebaseFinish(response, 5)
            }else{
                val response = Firebase().getDocument(reportedDrugs)
                profileListener?.onFirebaseFinish(response, 5)
            }
        }else{
            profileListener?.onError("Synchronization stopped, Please check internet connection")
        }
    }
    fun processReportedDrugs(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                if(obj.getBoolean(active)) {
                    val reportedBugs = ReportedDrugs(
                        null,
                        obj.optString(UniqueId),
                        obj.getInt(Age),
                        obj.optString(Gender),
                        obj.optString(
                            Hospital
                        ),obj.optString(Diagnosis),
                        obj.optString(PatientSetting),
                        obj.optString(Route),
                        obj.optString(Drugs),
                        obj.optString(Dose),
                        obj.optString(Duration),
                        obj.optString(Frequency),
                        Converter().timeStamptoDate(obj.optString(date_created)),
                        obj.optString(
                            created_by
                        ),
                        obj.getBoolean(active),
                        obj.optString(Amount)
                    )
                    Coroutines.main {
                        repository.checkSaveDrugs(reportedBugs)
                    }
                }
            }
            val todayDOB = Date(Calendar.getInstance().timeInMillis)
            prefs.savePreference(Sync,todayDOB.toString())
            profileListener?.onSuccess("done")
        }else{
            profileListener?.onError("Synchronization stopped, Please check internet connection")
        }
    }
    fun processUsers(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val auth =
                    com.majoriebash.bugsndrugs.data.db.entities.Users(
                        null,
                        obj.getString(name),
                        obj.getString(organization),
                        obj.getString(address_org),
                        obj.getString(profession_org),
                        obj.getString(state_org),
                        obj.getString(user_name).trim(),
                        obj.getString(lgas),
                        obj.getString(roleId),
                        obj.getString(privilege),
                        obj.optString(phone),
                        Converter().timeStamptoDate(obj.getString(date_created)),
                        obj.getBoolean(activated)
                    )
                Coroutines.main {
                    repository.checkSaveUser(auth)}
            }
            val response = Firebase().getDocument(reportedBugs)
            profileListener?.onFirebaseFinish(response, 4)
        }else{
            profileListener?.onError("Synchronization stopped, Please check internet connection")
        }
    }
    fun clearPref(){
        prefs.deleteAll()
    }
}