package com.majoriebash.bugsndrugs.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.majoriebash.bugsndrugs.data.db.entities.BugCheck
import com.majoriebash.bugsndrugs.data.db.entities.ReportCheck
import com.majoriebash.bugsndrugs.data.db.entities.ReportedBugs

@Dao
interface ReportedBugsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(rBugs: ReportedBugs) : Long

    @Query("SELECT * FROM ReportedBugs WHERE Active=:uid")
    fun getBug(uid:Boolean) : LiveData<List<ReportedBugs>>

    @Query("UPDATE ReportedBugs SET Active = :active WHERE Unique_Id=:uid")
    suspend fun delete(uid:String?,active:Boolean?)

    @Query("UPDATE ReportedBugs SET Age = :age, Hospital_Number = :hospital, Diagnosis=:diagnosis WHERE Unique_Id=:uid")
    suspend fun update(uid:String?,age:Int?,hospital:String?,diagnosis:String?)

    @Query("SELECT * FROM ReportedBugs WHERE ReportedBugs.Unique_Id=:uid")
    fun getBugs(uid:String?) : LiveData<ReportedBugs>

    @Query("SELECT ReportedBugs.*, users.institution as institution,users.number as phone,users.lga as lga, users.state as state, users.address as address, users.profession as profession FROM ReportedBugs LEFT OUTER JOIN users ON Created_By = Users.email WHERE Active=:uid")
    fun getBugsForCSV(uid:Boolean) : LiveData<List<ReportCheck>>

    @Query("SELECT COUNT(*) FROM ReportedBugs WHERE Active=:uid")
    fun getBugsCount(uid:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM ReportedBugs WHERE Gender=:uid AND Active=:active")
    fun getBugsMale(uid:String,active:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM ReportedBugs WHERE Gender=:uid AND Active=:active")
    fun getBugsFemale(uid:String,active: Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) as Total, dateCreated as bugsDate FROM ReportedBugs WHERE dateCreated<=DATE('now','-7 days') AND Active=:uid GROUP BY dateCreated")
    fun getGroupBug(uid:Boolean) : LiveData<List<BugCheck>>

    @Query("SELECT COUNT(*) FROM ReportedBugs WHERE Unique_Id = :uid")
    suspend fun getSingle(uid:String?) : Int
}