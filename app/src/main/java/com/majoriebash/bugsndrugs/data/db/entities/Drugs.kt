package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "drugs")
data class Drugs (
    @PrimaryKey(autoGenerate = false)
    var id: String = "",
    var name: String? = null,
    var desc:String? = null,
    var type:String? = null
)