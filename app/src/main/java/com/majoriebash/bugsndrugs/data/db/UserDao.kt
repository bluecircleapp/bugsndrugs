package com.majoriebash.bugsndrugs.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.majoriebash.bugsndrugs.data.db.entities.Auth
import com.majoriebash.bugsndrugs.data.db.entities.Users

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(users: Users) : Long

    @Query("SELECT * FROM users")
    fun getUsers() : LiveData<List<Users>>

    @Query("SELECT COUNT(*) FROM users WHERE email = :uid")
    suspend fun getSingle(uid:String?) : Int
}