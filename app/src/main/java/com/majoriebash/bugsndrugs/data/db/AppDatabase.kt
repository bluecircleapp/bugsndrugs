package com.majoriebash.bugsndrugs.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.majoriebash.bugsndrugs.data.db.entities.*
import com.majoriebash.bugsndrugs.util.DateConverter


@Database(
    entities = [Auth::class,Drugs::class, Bugs::class, ReportedBugs::class,ReportedDrugs::class,Users::class],
    version = 3, exportSchema = true
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getAuthDao(): AuthDao
    abstract fun getUserDao(): UserDao
    abstract fun getBugsDao(): BugsDao
    abstract fun getDrugsDao(): DrugsDao
    abstract fun getReportedBugsDao(): ReportedBugsDao
    abstract fun getReportedDrugsDao(): ReportedDrugsDao

    companion object {

        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        /*private val MIGRATION_3_4: Migration =
            object : Migration(3, 4) {
                override fun migrate(database: SupportSQLiteDatabase) {

                    database.execSQL(
                        "ALTER TABLE ReportedDrugs "
                                + "Add COLUMN Patient_Setting TEXT NULL"
                    )
                    database.execSQL(
                        "ALTER TABLE ReportedDrugs "
                                + "Add COLUMN Route TEXT NULL"
                    )

                }
            }*/

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "MBash.db").fallbackToDestructiveMigration().build()
    }
}