package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.*
import java.sql.Date

@Entity(tableName = "ReportedDrugs"
)
data class ReportedDrugs (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "Unique_Id")
    var uniqueId:String? = null,
    @ColumnInfo(name = "Age")
    var age:Int? = null,
    @ColumnInfo(name = "Gender")
    var gender:String? = null,
    @ColumnInfo(name = "Hospital_Number")
    var hospitalNumber:String? = null,
    @ColumnInfo(name = "Diagnosis")
    var diagnosis:String? = null,
    @ColumnInfo(name = "Patient_Setting")
    var patientSetting:String? = null,
    @ColumnInfo(name = "Route")
    var route:String? = null,
    @ColumnInfo(name = "Drugs")
    var drugs:String? = null,
    @ColumnInfo(name = "Dose")
    var dose:String? = null,
    @ColumnInfo(name = "Duration")
    var duration:String? = null,
    @ColumnInfo(name = "Frequency")
    var frequency:String? = null,
    var dateCreated: Date? = null,
    @ColumnInfo(name = "Created_By")
    var createdBy:String? = null,
    @ColumnInfo(name="Active")
    var active:Boolean? = null,
    @ColumnInfo(name="Amount")
    var amount:String? = null
)