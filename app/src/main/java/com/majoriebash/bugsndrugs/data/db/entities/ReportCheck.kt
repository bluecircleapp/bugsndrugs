package com.majoriebash.bugsndrugs.data.db.entities

import androidx.room.Embedded


data class ReportCheck (
    @Embedded
    val reported: ReportedBugs,
    val institution: String?,
    val phone: String?,
    val lga:String?,
    val state:String?,
    val address:String?,
    val profession:String?
)